#include <ctime>

//#include "board.h"
#include "game.h"

int main()
{
	game game(6,7);
	//board board(7,6);
	//game game(board.get_rows(), board.get_cols());  
	game.print_board();
	srand(time(NULL));
	int slot;
	while (!game.over){
		//begin round
		cout << "player 1 go" << endl;
		cout << "player 1 input:";
		cin >> slot;  //get user input
		while (slot < 1 || slot > game.get_cols() || game.is_slot_filled(slot-1) == true){
			cout << "slot unavailable, please choose another" << endl;
			cout<< "player 1 input:";
			cin >> slot;	
		}	
		game.insert_red_token(slot-1);
		if (game.is_game_over() == true){
			game.print_board();
			cout << "player 1 has won" << endl;
			game.over = true;
			break;
		}
		/*if (board.max() == 4){
			cout << "player 1 wins!" << endl;
			break;
		}		*/

		game.print_board();
		cout << "player 2 go" << endl;
		slot = rand() % game.get_cols();  //AI
		game.insert_yellow_token(slot-1);
		game.print_board();
		if (game.is_game_over() == true){
			game.print_board();
			cout << "player 2 has won" << endl;
			game.over = true;
		}
		//end round
	} 
	return 0;
}
