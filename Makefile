CC = g++ -std=c++11
CFLAGS = -Wall -g
#LDFLAGS

OBJECTS = board.o game.o

main: main.cc board.h game.h $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) main.cc

test: main.cc board.h game.h $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) main.cc
	./a.out

board.o: board.cc board.h
	$(CC) $(CFLAGS) -c board.cc

game.o: game.cc game.h
	$(CC) $(CFLAGS) -c game.cc
