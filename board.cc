#include "board.h"

board::board(int rows, int cols) : rows{rows}, cols{cols} {
	cout << "constructing board" << endl;
}

board::~board(){
	cout << "destroying board" << endl;
};

void board::print_board(){
	for (int i=0; i<rows; i++){
		for (int j=0; j<cols; j++){
			cout << "|  ";
		}
		cout << "|" << endl;
	}
	for (int j=0; j<cols; j++)
		cout << "===";
	cout << "=" << endl;
	for (int j=0; j<cols; j++)
		cout << " " << j+1 << " ";
	cout << endl;
}

int board::get_cols(){
	return cols;
}

int board::get_rows(){
	return rows;
}

/*int board::get_slot(){
	return slot;
}*/
