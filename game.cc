#include "game.h"

game::game(int rows, int cols) : board(rows,cols), rows{rows}, cols{cols}, sz{rows*cols}, num_in_slot{new int[cols]},  layout{new char[sz]} {
	p1_move = 0;
	p2_move = 0;
	round = 1;
	over = false;  //game is initially not over
	for (int j=0; j<cols; j++){
		for (int i=0; i<rows; i++){
			layout[i*cols + j] = 'O';  //empty game at start
		num_in_slot[j+1] = 0;
		}
	}
}

game::~game(){
	cout << "destroying game" << endl;
	delete[] layout;
	delete[] num_in_slot;
};

void game::print_board(){
	for (int i=0; i<rows; i++){
		for (int j=0; j<cols; j++){
			if (layout[i*cols + j] == 'R'){
			//if (round == i-1 && p1_move == j-1) //put safeguard here
				cout << "|\033[1;31m *\033[0m";
			}
			else if (layout[i*cols + j] == 'Y'){
			//if (round == i-1 && p1_move == j-1) //put safeguard here
				cout << "|\033[1;33m *\033[0m";
			}
			else
				cout << "|  ";
		}
		cout << "|" << endl;
	}
	for (int j=0; j<cols; j++)
		cout << "===";
	cout << "=" << endl;
	for (int j=0; j<cols; j++)
		cout << " " << j+1 << " ";
	cout << endl;
}

void game::insert_red_token(int slot){
	p1_move = slot;
	layout[((rows-1) - num_in_slot[slot])*cols + slot] = 'R';
	num_in_slot[slot]++;  //don't need slot-1 since array is one longer than needs to be
}
void game::insert_yellow_token(int slot){
	p2_move = slot;
	layout[((rows-1) - num_in_slot[slot])*cols + slot] = 'Y';
	num_in_slot[slot]++;
}

int game::get_cols(){
	return cols;
}

int game::get_rows(){
	return rows;
}

int game::max(){
	return 0;	
}

bool game::is_slot_filled(int slot){
	if (layout[((rows-1) - num_in_slot[slot])*cols + slot] == 'O')
		return false;
	else
		return true;
}

bool game::is_game_over(){
	char color;
	int count;
	int up, down, left, right;

#if 0
	for (int i=0; i<rows; i++){
		for (int j=0; j<cols; j++){
			if (layout[i*cols + j] != 'O'){
				count++;
				color = layout[i*cols + j];
				while ( count < 4 ){
					for (int k=-1; k<=1; k++){
						for (int l=-1; l<=1; l++){
							if (layout[(i+k)*cols + (j+l)] == color){
								//go in that direction...
							}
						}
					}
				}
			}
		}
	}
#endif
	//check verticals
	for (int i=0; i<rows-3; i++){
		for (int j=0; j<cols; j++){
			count = 0;
			down=1;
			if (layout[i*cols + j] != 'O'){
				count++;
				color = layout[i*cols + j];
				while (count != 4){
					if (layout[(i+down)*cols + j] == color){
						count++; 
						down++;
					}
				}
				if (count == 4);
					return true;
			}
		}
	}
	count = 0;
	color='O';
/*	//check horizontals
	for (int i=0; i<rows; i++){
		for (int j=0; j<cols-3; j++){
			count = 0;
			right = 1; 
//			cout << "right is set to " << right << "for i = " << i << " and j = " << j << endl;
			if (layout[i*cols + j] != 'O'){
				count++;
				color = layout[i*cols + j];
				while (count != 4){
					if (layout[i*cols + (j+right)] == color){
						count++; 
						right++;
					}
				} 
				if (count == 4);
					return true;
			}
		}
	}
	count = 0;
	color='O';
	//first check for / diagonals
	for (int i=3; i<rows; i++){
		for (int j=0; j<cols-4; j++){
			up = 1; right = 1;
			count = 0;
			if (layout[i*cols + j] != 'O'){
				color = layout[i*cols + j];
				count++;
				while (count != 4 && layout[(i-up)*cols + (j+right)] == color){
					count++; up++; right++; 
				}
				if (count == 4)
					return true;
			}
		}
	}
	count = 0;
	color='O';
	//now check for \diagonals 
	for (int i=0; i<rows-4; i++){
		for (int j=0; j<cols-4; j++){
			count = 0;
			down = 1; right = 1;
			if (layout[i*cols + j] != 'O'){
				color = layout[i*cols + j];
				count++;
				while (down < 4 && right < 4){
					if(layout[(i+down)*cols + (j+right)] == color){
						count++; down++; right++; 
						if (count == 4){
							cout << "count = " << count << ", down = " << down << ", right = " << right << endl;
							return true;
						}
					}
					else
						down = 5;  //break while loop
				}
			}
		}
	}
*/	return false;
}
