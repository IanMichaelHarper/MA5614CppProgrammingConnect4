#include <iostream>

using namespace std;

class board{
	private:
		int rows;
		int cols;
	public:
		board(int rows, int cols);  //constructor
		~board(); //destructor  
		void print_board();
		int get_cols();
		int get_rows();
		int get_slot();
};
