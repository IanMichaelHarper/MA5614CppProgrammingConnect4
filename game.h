#include "board.h"

class game : public board {
	private:
		int rows;
		int cols;
		int p1_move;
		int p2_move;
		int sz;
		int round;
		int slot;
		int* num_in_slot;
		char* layout;
	public:
		bool over;  //game over
		game(int rows, int cols);  //constructor
		~game(); //destructor  
		void print_board();
		void insert_red_token(int slot);
		void insert_yellow_token(int slot);
		int get_cols();
		int get_rows();
		bool is_slot_filled(int slot);
		int max();
		bool is_game_over();
};
